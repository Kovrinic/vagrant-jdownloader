# Vagrant: JDownloader 2 Bionic VM
Vagrant provision scripts to create a JDowloader 2 (JD2) Ubuntu 18.04 (Bionic) virtual machine (VM).

## Requirements
- [Vagrant](https://www.vagrantup.com/downloads.html)
- [VirtualBox](https://www.virtualbox.org/) _(or an equivalent)_
- MyJDownloader Account _([register](https://my.jdownloader.org/login.html#register))_
- Vagrant box [bento/ubuntu-18.04](https://app.vagrantup.com/bento/boxes/ubuntu-18.04)


## Install

Clone this repository, then open the downloaded location.
```
git clone https://gitlab.com/Kovrinic/vagrant-jdownloader.git
cd vagrant-jdownloader
```

### Config
Set the custom local variables within the `Vagrantfile`.
```bash
JDMEDIA_ID		= 1001								# set UID/GID for path permissions
JDUSER			= "jdmedia"							# set JD2 media username
DL_HOST_DIR		= "/home/matthew/downloads/vmjd2/"	# set host download location
DL_VM_DIR		= "/mnt/jdmedia/downloads"			# set download location within VM
BOOTSH			= "bootstrap.sh"					# select bootstrap verison
```
Make sure to create the `DL_HOST_DIR` locally before trying to provision the VM.

`BOOTSH` gives two options, either `bootstrap.sh` or `bootstrap-ads.sh`, where the `-ads.sh` is the normal default JD2 install with ads in the GUI client.  The default is `bootstrap.sh` which is provided by JD2 as the Adware-free version.  However as a headless-server instance, we will not see the Ads within the GUI because we'll always interact with the [my.jdownloader](http://my.jdownloader.org/) web interface.

### UP
Once desired config is setup, then run the following to start the provision process:
```bash
vagrant up
```

Once vagrant finishes provisioning, JD2 will only be _half_ setup.  For JD2 to finish installing, we'll have to manually enter our JD2 _username_ and _password_.  Once confirmed, and all is well, then we'll enable the service to auto start on boot.

SSH into the newly created VM, from the same git directory:
```bash
vagrant ssh
```
Then start JD2 as our _jdmedia_ user in a **norestart** state:
```bash
sudo -u jdmedia java -jar /opt/jdownloader/JDownloader.jar -norestart
```

JD2 will continue to updated and initialize until it eventually ask for your MyJDownloader login information:
```bash
|---------------------------Headless Information-------------------------------
|	MyJDownloader Setup
|	Your 'My JDownloader' logins are not correct.
|	Please check username/email and password!
|	Enter y -> Enter Logins
|	Enter n -> Exit JDownloader
```
enter `y`, then type email
```bash
y
|	Please Enter your MyJDownloader Email:
|>
jd2user@example.com
```
Next enter password:
```bash
|	Please Enter your MyJDownloader Password(not visible):
|>

```

After JD2 finishes setting up, check that the new JD2 instances shows up at https://my.jdownloader.org and edit any settings as needed (i.e. _Device Name_, _package by subfolder_, etc...).

Back to our terminal, enter `Ctrl-c` to stop JD2, then enable the service to auto start on boot:
```bash
sudo systemctl enable jd2.service
```

then start the `systemctl` service.
```bash
sudo systemctl start jd2.service
```

## Wrapping Up
Do note that `1GB` of ram is not much for Java to run on, but should be a good start (increase as needed).  Also, pay attention to user:group directory permissions for the HOST download directory.  Take care in directory permissions when using a directory shared by other users.

### vagrant commands
To stop _(shutdown)_ the VM:
```bash
vagrant halt
```
To add a vagrant box:
```bash
vagrant box add bento/ubuntu-18.04
```
To reload the VM _(will skip provisioning unless specified)_:
```bash
vagrant reload
```
To **destroy** the VM _(i.e. we no longer want this VM, delete the VM)_:
```bash
vagrant destroy
```
There will be a warning, and will require further user interaction before deleting the VM.

### extras
For JD2 logging, while within the VM run the following to tail the `journalctl`:
```bash
sudo journalctl --utc -u jd2.service -f -n 50
```
