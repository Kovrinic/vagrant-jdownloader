#!/usr/bin/env bash
# install info: http://bit.ly/2wfgkDk
# Adware-free Version

###############################################################################
# Updates                                                                     #
###############################################################################
echo "[bootstrap]:  Updating apt"
sudo apt-get update


###############################################################################
# JDownloader new User setup                                                  #
###############################################################################
# Create new user:group so that JDownloader can run as separate user
#   Add system group/user, set ids
#   more info: http://bit.ly/2N06Uma
echo "[bootstrap]:  Setting up '$2' user:group"
sudo addgroup --gid $1 $2
sudo adduser --system --gecos "Media Service" \
  --disabled-password --uid $1 --gid $1 --home /var/lib/$2 $2

# Add vagrant user to our new user's group
# Allows the vagrant user to access the downloads
sudo gpasswd -a vagrant $2


###############################################################################
# Install(s)                                                                  #
###############################################################################
# double check wget is installed
echo "[bootstrap]:  Installing wget"
sudo apt-get install -y wget

# Java 8
#   Don't use headless, otherwise JD2 updates will freeze
#   more info: http://bit.ly/2N1Rm1q
echo "[bootstrap]:  Installing Java 8 JRE"
sudo apt-get -y install openjdk-8-jre

# MegaCMD
#   Will use to download Adware-free JDownloader 2
#   https://mega.nz/cmd
echo "[bootstrap]:  Installing MegaCMD"
wget -q "https://mega.nz/linux/MEGAsync/xUbuntu_18.04/amd64/megacmd-xUbuntu_18.04_amd64.deb"
sudo dpkg -i megacmd-xUbuntu_18.04_amd64.deb
sudo apt-get -fy install


###############################################################################
# JDownloader: Download & Setup                                               #
###############################################################################
# JDownloader 2 Adware-free Setup
#   more info: http://bit.ly/2N7kK6b
echo "[bootstrap]:  Downloading JDownloader"
mega-get "https://mega.nz/#!mctCnIhC!fNqx1UOf06cVB7h9Cis1Kk5J_tAGMTFsSQcuQ2FPfYw"

# Move JDownloader.jar and change the owner to the user;
#   under which JDownloader should be executed
echo "[bootstrap]:  Setting up JDownloader"
sudo mkdir -p /opt/jdownloader
sudo mv -n JDownloader.jar /opt/jdownloader
sudo chown -R $2:$2 /opt/jdownloader

# Initialize JDownloader install
#   Execute this command several times until
#   you get asked for your My JDownloader login credentials on the console.
echo "[bootstrap]:  Initializing JDownloader"
sudo -u $2 java -jar /opt/jdownloader/JDownloader.jar -norestart

# Creates a new systemd unit under /etc/systemd/system/jd2.service
#   and paste this script
#   fix start hang: http://bit.ly/2BtQoJS
echo "[bootstrap]:  Creating jd2.service"

cat <<EOF > /etc/systemd/system/jd2.service
[Unit]
Description     = JDownloader Service
After           = network.target

[Service]
Environment     = JD_HOME="/opt/jdownloader"
Type            = simple
ExecStart       = /usr/bin/java -Djava.awt.headless=true -jar /opt/jdownloader/JDownloader.jar
RemainAfterExit = yes
User            = $2
Group           = $2

[Install]
WantedBy        = multi-user.target
EOF


###############################################################################
# Wrapping Up                                                                 #
###############################################################################
# After logging in,
#   make sure to run the following comand to enable JDownloader to autostart
echo "[bootstrap]:  Run 'vagrant ssh' then do the following..."
echo "[bootstrap]:  Execute this command several times until you get asked for your My JDownloader login credentials on the console."
echo "[bootstrap]:    sudo -u $2 java -jar /opt/jdownloader/JDownloader.jar -norestart"
echo "[bootstrap]:  After JDownloader finishes setting up, make sure to run the following comand to enable JDownloader to autostart"
echo "[bootstrap]:    sudo systemctl enable jd2.service"
echo "[bootstrap]:  Then start jd2.service"
echo "[bootstrap]:    sudo systemctl start jd2.service"

# done
echo "[bootstrap]:  Finished!"
